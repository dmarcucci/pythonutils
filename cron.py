from crontab import CronTab  # pip install python-crontab
from datetime import datetime, timedelta
import random
import sys

command = sys.argv[1]

now = datetime.now()
future = None

if sys.argv[2] == "daily":
    future = now + timedelta(days=1)
elif sys.argv[2] == "hourly":
    future = now + timedelta(hours=1) + timedelta(minutes=(random.randint(int(sys.argv[3]), int(sys.argv[4]))))
elif sys.argv[2] == "reload":
    future = now + timedelta(minutes=1)
elif sys.argv[2] == "retry":
    future = now + timedelta(minutes=5)
elif sys.argv[2] == "every3hours":
    future = now + timedelta(hours=3) + timedelta(minutes=(random.randint(int(sys.argv[3]), int(sys.argv[4]))))

month = future.month
day = future.day
hour = random.randint(int(sys.argv[3]), int(sys.argv[4])) if sys.argv[2] == "daily" else future.hour
minute = future.minute

cron = CronTab(user=True)

for job in cron:
    if job.command == command:
        cron.remove(job)
        break

job = cron.new(command=command)
job.month.on(month)
job.day.on(day)
job.hour.on(hour)
job.minute.on(minute)

job.is_valid()

cron.write()

print(job)
