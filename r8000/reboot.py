from lxml import html  # sudo apt-get install libxml2-dev libxslt-dev python-dev python-lxml
import requests  # pip install requests
import re
import base64
import json

host = 'http://routerlogin.net'
file = open('credentials.json')  # Format: {"user": <user>, "password": <password>}
credentials = json.load(file)

data = credentials['user'] + ':' + credentials['password']
file.close()
auth = 'Basic ' + str(base64.b64encode(data.encode('utf-8')))

headers = {'Authorization': auth, 'Content-Type': 'application/x-www-form-urlencoded'}
login = requests.get(host, headers=headers)
status = login.status_code

if status == 401:
    cookies = login.headers['Set-Cookie']

    if 'XSRF_TOKEN=' in cookies:
        cookies = cookies.split('; ')

        for element in cookies:
            cookie = element.split('=')
            if cookie[0] == 'XSRF_TOKEN' and len(cookie) == 2:
                xsrf_token = cookie[1]
                pattern = re.compile("[0-9]+")

                if pattern.match(xsrf_token):
                    cookies = dict(XSRF_TOKEN=xsrf_token)
                    login = requests.get(host, headers=headers, cookies=cookies)

try:
    login.raise_for_status()
    page = requests.get(host + '/ADV_home2_r8000.htm', headers=headers)
    tree = html.fromstring(page.content)
    action = tree.xpath('/html/body/form[1]/@action')[0]

    if action.startswith('newgui_adv_home.cgi?id=') and not ('&' in action):
        identity = action[23:]
        payload = {'buttonSelect': 2, 'wantype': 'dhcp', 'enable_apmode': 1}
        url = host + '/newgui_adv_home.cgi?id=' + identity
        reboot = requests.post(url, data=payload, headers=headers)
        status = reboot.status_code

        if status == 200:
            print("The router is rebooting. It might take up to two minutes for your changes to take effect.")
        else:
            print("Reboot unsuccessful")
    else:
        print("Unable to get id for reboot call")
except requests.exceptions.HTTPError as error:
    print("Received the following HTTPError - " + str(error))
except:
    print("An unknown exception occurred")
