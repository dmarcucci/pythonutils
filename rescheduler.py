from crontab import CronTab  # pip install python-crontab
from datetime import datetime, timedelta
import json

cron = CronTab(user=True)


def is_in_past(job_to_check):
    now = datetime.now()
    year = now.year
    month = int(str(job_to_check.month))
    day = int(str(job_to_check.day))
    hour = int(str(job_to_check.hour))
    minute = int(str(job_to_check.minute))
    schedule = datetime(year, month, day, hour, minute)
    return schedule < now


def reschedule_job(job_to_change):
    command = job_to_change.command
    cron.remove(job_to_change)
    runtime = datetime.now() + timedelta(minutes=5)
    new_job = cron.new(command=command)
    new_job.month.on(runtime.month)
    new_job.day.on(runtime.day)
    new_job.hour.on(runtime.hour)
    new_job.minute.on(runtime.minute)
    new_job.is_valid()


def get_excluded_job_commands():
    with open('exclusions.json') as json_file:
        exclusions = json.load(json_file)
    return exclusions


excluded_jobs = get_excluded_job_commands()

for job in cron:
    if job.command not in excluded_jobs and is_in_past(job):
        reschedule_job(job)

cron.write()
