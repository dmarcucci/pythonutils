import json
import psutil  # pip install psutil
import subprocess

whitelist = open('whitelist.json')
whitelist = json.load(whitelist)

# Remove python if included as we have special handling below
if "python" in whitelist:
    print("Removing python from list")
    whitelist.remove("python")

processes = psutil.process_iter()

check = False

for process in processes:
    try:
        if process.name() in whitelist or (process.name() == "python" and "reboot.py" not in process.cmdline()):
            check |= True
            break
    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
        pass

if not check:
    reboot = subprocess.Popen(["sudo", "reboot"], stdout=subprocess.PIPE)
    print(reboot.communicate()[0])
else:
    print("Not rebooting at this time")

